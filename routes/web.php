<?php

Route::get('products', 'ProductController@getProducts')->name('products');

Route::get('/', function () {
    return redirect('products');
});

