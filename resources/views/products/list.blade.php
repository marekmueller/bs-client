<!DOCTYPE html>
<html>
<head>
    <title>Products list</title>
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
</head>
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="{{ route('products') }}">Products on stock</a></li>
                    <li><a href="{{ route('products', ['amount' => 0]) }}">Products out of stock</a></li>
                    <li><a href="{{ route('products', ['amount' => 5]) }}">Products more of 5 pcs.</a></li>
                </ul>
            </div>
        </div>
    </nav>

    @if($errors)
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">{{ $error }}</div>
        @endforeach
    @endif

    <div class="container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Product name</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                @foreach($products as $item)
                    <tr>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->amount }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>