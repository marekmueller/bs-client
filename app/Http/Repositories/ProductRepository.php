<?php

namespace App\Http\Repositories;

use App\Http\Contracts\ProductRepositoryInterface;
use App\Http\Repositories\AbstractHttpRepository;

class ProductRepository extends AbstractHttpRepository implements ProductRepositoryInterface
{
    public function getAllByAmount($amount)
    {
        $response = $this->client->get('products', [
            'query' => ['amount' => $amount]
        ]);

        return json_decode($response->getBody());
    }
}