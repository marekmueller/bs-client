<?php

namespace App\Http\Repositories;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

abstract class AbstractHttpRepository
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => env('API_URL', 'http://api.bs.exp/api/v1/')
        ]);
    }
}