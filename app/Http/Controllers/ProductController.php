<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Services\ProductService;
use App\Http\Repositories\ProductRepository;
use App\Http\Requests\GetProductsRequest;

class ProductController extends Controller
{
    private $productService;

    public function __construct()
    {
        $this->productService = new ProductService(new ProductRepository);
    }

    public function getProducts(GetProductsRequest $request)
    {
        $products = $this->productService->getProducts($request->amount);

        return view('products.list', compact('products'));
    }
}
