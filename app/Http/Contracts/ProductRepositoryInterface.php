<?php

namespace App\Http\Contracts;

interface ProductRepositoryInterface
{
    public function getAllByAmount($amount);
}