<?php

namespace App\Http\Services;

use App\Http\Contracts\ProductRepositoryInterface;

class ProductService
{
    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getProducts($amount)
    {
        return $this->productRepository->getAllByAmount($amount);
    }
}